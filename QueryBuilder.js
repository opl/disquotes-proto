const Sql = require('sql').Sql;

var QueryBuilder = module.exports = function() {
	this.builder = new Sql('mysql', {});

	this.quote = this.builder.define({
		name: 'quote',
		columns: ['id', 'title', 'authorID', 'authorName', 'serverID', 'serverName', 'added']
	});

	this.message = this.builder.define({
		name: 'message',
		columns: ['id', 'quoteID', 'messageID', 'authorID', 'authorName', 'authorColor', 'avatarHash', 'text', 'added', 'edited']
	});
}
