const Message = require('./Message');

var Quote = module.exports = function Quote(data) {
	this.id = data.id;
	this.title = data.title;
	this.authorID = data.authorID;
	this.authorName = data.authorName;
	this.serverID = data.serverID;
	this.serverName = data.serverName;
	this.added = new Date(data.added);
	this.messages = data.messages || null;
}

Quote.prototype.toPublicJSON = function() {
	var msgs = null;

	if (this.messages instanceof Array) {
		msgs = [];
		for (var i = 0; i < this.messages.length; i++) msgs[i] = this.messages[i].toPublicJSON();
	}

	return {
		id: this.id,
		title: this.title,
		authorID: this.authorID,
		authorName: this.authorName,
		serverID: this.serverID,
		serverName: this.serverName,
		added: this.added.getTime(),
		messages: msgs
	}
}

Quote.getByID = function(req, id, callback) {
	req.db.query('SELECT * FROM `quote` WHERE `id` = ?', [id], function(err, fields) {
		if (err) return callback(err);

		if (fields.length === 0) return callback(null, null);

		Message.getByQuoteID(req, fields[0].id, function(err, messages) {
			if (err) return callback(err);

			fields[0].messages = messages;

			return callback(null, new Quote(fields[0]));
		});
	});
}

Quote.getQuotes = function(req, options, callback) {
	options = options || {};

	var page = parseInt(options.page) == options.page ? parseInt(options.page) : 0;
	var limit = parseInt(options.limit) == options.limit ? parseInt(options.limit) : 25;
	var newestFirst = typeof(options.newestFirst) === 'boolean' ? options.newestFirst : false;

	var quoteTable = req.queryBuilder.quote;
	var messageTable = req.queryBuilder.message;

	var qs = quoteTable.subQuery('quote').select(quoteTable.star()).order(newestFirst ? quoteTable.added.descending : quoteTable.added.ascending).limit(limit).offset(page * limit);

	if (typeof(options.where) === 'object') {
		if (options.where.serverID) qs.where(quoteTable.serverID.equals(options.where.serverID));
	}

	var query = quoteTable.select(
		messageTable.star(), quoteTable.id.as('qID'), quoteTable.title, quoteTable.authorID.as('qAuthorID'), quoteTable.authorName.as('qAuthorName'), quoteTable.serverID, quoteTable.serverName, quoteTable.added.as('qAdded')
	).from(qs.leftJoin(messageTable).on(quoteTable.id.equals(messageTable.quoteID))).order(newestFirst ? quoteTable.added.descending : quoteTable.added.ascending).toQuery();

	req.db.query(query.text, query.values, function(err, fields) {
		if (err) return callback(err);

		if (fields.length === 0) return callback(null, []);

		var quotes = [];
		var quote = null;

		for (var m of fields) {
			if (!quote || quote.id !== m.qID) {
				if (quote) quotes.push(quote);

				quote = new Quote({
					id: m.qID,
					title: m.title,
					authorID: m.qAuthorID,
					authorName: m.qAuthorName,
					serverID: m.serverID,
					serverName: m.serverName,
					added: m.qAdded
				});
				quote.messages = [];
			}

			quote.messages.push(new Message(m));
		}
		quotes.push(quote);

		return callback(null, quotes);
	});
}
