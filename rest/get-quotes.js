const Quote = require('./../model/Quote');

module.exports = function(rest) {
	rest.get('/quotes(/:page)?', rest.getRequestValidator({
		params: {
			type: 'integer',
			name: 'page',
			min: 0,
			optional: true
		},
		query: {
			type: 'and',
			value: [{
				type: 'integer',
				name: 'page',
				min: 0,
				optional: true
			}, {
				type: 'integer',
				name: 'limit',
				min: 1,
				max: 60,
				optional: true
			}]
		}
	}), function(req, res, next) {
		Quote.getQuotes(req, {
			page: typeof(req.params.page) !== 'undefined' ? parseInt(req.params.page) : parseInt(req.query.page),
			limit: parseInt(req.query.limit),
			newestFirst: true
		}, function(err, quotes) {
			if (err) return next(err);

			for (var i = 0; i < quotes.length; i++) quotes[i] = quotes[i].toPublicJSON();

			res.json({
				status: 'success',
				data: quotes
			});
		});
	});
}
