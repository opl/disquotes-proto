$(function() {
	function renderQuote(data) {
		var added = new Date(data.added);

		var element = $('<section class="quote">');

		element.append(
			$('<a class="info has-ripple-effect">').attr('href', '/q/' + data.id).append(
				$('<div class="title">').text(data.title)
			).append(
				$('<div class="details">').append(
					$('<span class="server-name" title="Server name">').attr('title', 'Server name').text(data.serverName)
				).append(
					$('<span>').html(' &#8226; ')
				).append(
					$('<span class="author">').text('Added by ' + data.authorName)
				).append(
					$('<span>').html(' &#8226; ')
				).append(
					$('<span class="timestamp" title="Added">').text(added.toLocaleDateString() + ' ' + added.toLocaleTimeString())
				)
			)
		).append(
			messages = $('<div class="messages">')
		);

		var textContainer, lastUserID;

		for (let i = 0; i < data.messages.length; i++) {
			var msg = data.messages[i];

			if (msg.authorID !== lastUserID) {
				var added = new Date(msg.added);

				messages.append(
					$('<div class="message">').append(
						$('<div class="avatar">').css('background-image', 'url(https://cdn.discordapp.com/avatars/' + encodeURI(msg.authorID) + '/' + encodeURI(msg.avatarHash) + '.jpg)')
					).append(
						textContainer = $('<div class="message-content">').append(
							$('<div class="header">').append(
								$('<span class="username">').css('color', msg.authorColor === null ? undefined : '#' + msg.authorColor.toString(16)).text(msg.authorName)
							).append(
								$('<span class="timestamp">').text(added.toLocaleDateString() + ' ' + added.toLocaleTimeString())
							)
						)
					)
				);
			}

			lastUserID = msg.authorID;

			textContainer.append(
				$('<div class="text">').text(msg.text)
			);
		}

		return element;
	}

	function loadQuotes(page, callback) {
		var quotesContainer = $('#quote-list');

		if (quotesContainer.data('loading') === 'true') return;

		quotesContainer.data('loading', 'true');

		$.get('/_/quotes/' + page).done(function(data) {
			quotesContainer.data('page', (parseInt(quotesContainer.data('page')) || 0) + 1);

			if (data.data.length === 0) quotesContainer.data('end', 'true');

			for (var q of data.data) quotesContainer.children('.container').append(renderQuote(q));

			quotesContainer.data('loading', 'false');

			checkScroll();
		}).fail(function(err) {
			console.error(err);
		});
	}

	function checkScroll() {
		var quotesContainer = $('#quote-list');

		if (quotesContainer.scrollTop() > quotesContainer.children('.container').height() - $(window).height() - 100 && quotesContainer.data('end') !== 'true') {
			loadQuotes(quotesContainer.data('page') || '0');
		}
	}

	$('#quote-list').on('scroll', checkScroll);

	loadQuotes(0);

	$('#lost .go-back').click(function() {
		history.back();
	});


	page('/(p/:page)?', function(ctx) {
		$('#quote-list').addClass('show');
		document.title = 'Disquotes';
	});
	page.exit('/(p/:page)?', function(ctx, next) {
		$('#quote-list').removeClass('show');
		next();
	});

	page('/q/:quoteID', function(ctx, next) {
		if (parseInt(ctx.params.quoteID) != ctx.params.quoteID) return next();

		var quoteView = $('#quote-view');

		quoteView.addClass('show').children('.container').empty();
		document.title = 'Disquotes';
		$('#app-header>.header-button').attr('href', '/').children('.material-icons').text('arrow_back');

		$.get('/_/quote/' + ctx.params.quoteID).done(function(data) {
			document.title = data.data.title + ' - Disquotes';
			quoteView.children('.container').append(renderQuote(data.data));
		}).fail(function(err) {
			console.error(err);
		});
	});
	page.exit('/q/:quoteID', function(ctx, next) {
		$('#quote-view').removeClass('show');
		$('#app-header>.header-button').attr('href', null).children('.material-icons').text('');
		next();
	});

	page('/about', function(ctx) {
		$('#about').addClass('show');
		document.title = 'About - Disquotes';
		$('#app-header>.header-button').attr('href', '/').children('.material-icons').text('arrow_back');
	});
	page.exit('/about', function(ctx, next) {
		$('#about').removeClass('show');
		$('#app-header>.header-button').attr('href', null).children('.material-icons').text('');
		next();
	});

	page('*', function() {
		$('#lost').addClass('show');
		document.title = 'Page Not Found - Disquotes';
	});
	page.exit('*', function(ctx, next) {
		$('#lost').removeClass('show');
		next();
	});

	page();
});
