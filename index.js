const express = require('express');
const http = require('http');
const https = require('https');
const mysql = require('mysql');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
const initBot = require('./bot');

function merge(a, b) {
	for (var i in b) {
		if (typeof(a[i]) === 'undefined') {
			a[i] = b[i];
		} else if (typeof(a[i]) === 'object') {
			if (typeof(b[i]) === 'object') merge(a[i], b[i]);
		}
	}

	return a;
}

var config = merge(require('./config/config.json'), require('./default-config.json'));

var mysqlPool = mysql.createPool(config.mysql);

mysqlPool.begin = function(callback) {
	mysqlPool.getConnection(function(err, conn) {
		if (err) return callback(err);

		conn.query('BEGIN', function(err) {
			if (err) return callback(err);

			var commit = function(errOverride, callback) {
				if (!callback) {
					callback = errOverride;
					errOverride = undefined;
				}

				conn.query('COMMIT', function(err) {
					if (err) return rollback(function() {
						return callback(err);
					});

					conn.release();

					return callback(null);
				});
			};

			var rollback = function(errOverride, callback) {
				if (!callback) {
					callback = errOverride;
					errOverride = undefined;
				}

				conn.query('ROLLBACK', function(err) {
					conn.release();

					if (err) return callback(errOverride || err);

					return callback(errOverride);
				});
			};

			return callback(null, conn, commit, rollback);
		});
	});
};

var app = express();

app.set('x-powered-by', false);
app.set('trust proxy', config.http.trustProxy);

var rest = new express.Router();

rest.mysqlPool = mysqlPool;
rest.queryBuilder = new (require('./QueryBuilder'))();
rest.getRequestValidator = require('./rest-helper').getRequestValidator;

rest.use('/res', express.static(path.join(__dirname, 'res')));

rest.use('*', function(req, res, next) {
	req.queryBuilder = rest.queryBuilder;

	mysqlPool.getConnection(function(err, conn) {
		if (err) return next(err);

		conn.query('BEGIN', function(err) {
			if (err) return next(err);

			req.db = conn;

			var sendJSON = res.json;
			res.json = function(json) {
				if (req.db) {
					var args = arguments;

					req.db.query(json.status === 'success' ? 'COMMIT' : 'ROLLBACK', function(err) {
						if (err) return next(err);

						try {req.db.release();}
						catch (ex) {}

						sendJSON.apply(res, args);
					});
				} else {
					sendJSON.apply(res, arguments);
				}
			};

			next();
		});
	});
});

var jsonBodyParser = bodyParser.json();

rest.post('*', jsonBodyParser);
rest.put('*', jsonBodyParser);

for (var f of fs.readdirSync(path.join(__dirname, 'rest'))) require(path.join(__dirname, 'rest', f))(rest);

rest.use(function(err, req, res, next) {
	console.error(err.stack); // TODO: log to file

	if (req.db) {
		req.db.query('ROLLBACK', function(err) {
			req.db.release();
		});
	}

	res.status(500).json({
		status: 'serverError'
	});
});

rest.use(function(req, res) {
	res.status(404).json({});
});

app.use('*', function(req, res, next) {
	console.log(req.ip + ' ' + req.method + ' ' + req.originalUrl);
	next();
});

app.use('/_', rest);

app.get('*', function(req, res) {
	res.sendFile(path.join(__dirname, 'index.html'));
})

app.use(function(err, req, res, next) {
	console.error(err);
	res.status(500).send('Server fault');
});

var httpServer = http.createServer(app);
var httpsServer = null;

httpServer.listen(config.http.port, config.http.hostname);

if (config.http.https) {
	httpsServer = https.createServer({
		cert: fs.readFileSync(path.isAbsolute(config.http.https.public) ? config.http.https.public : path.join(__dirname, config.http.https.public)),
		key: fs.readFileSync(path.isAbsolute(config.http.https.private) ? config.http.https.private : path.join(__dirname, config.http.https.private))
	}, app);
	httpsServer.listen(config.http.https.port, config.http.https.hostname);
} else {
	console.warn('\x1b[31m== STARTING WITH NO SSL ==\x1b[39m');
}

initBot(config, mysqlPool, rest.queryBuilder);
