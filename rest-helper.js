function capitalize(str) {
	return str[0].toUpperCase() + str.substr(1);
}

function toStatus(result) {
	if (['tooSmall', 'tooBig', 'tooShort', 'tooLong'].indexOf(result.cause) !== -1) return result.name + capitalize(result.cause);
	else return result.cause + capitalize(result.name);
}

function checkNode(node, obj) {
	var cause = null;
	
	if (node.type === 'and') {
		for (var c of node.value) {
			var result = checkNode(c, obj);

			if (!result.pass) return result;
		}
	} else if (node.type === 'or') {
		var result = null;

		for (var c of node.value) {
			var r = checkNode(c, obj);

			if (result.pass) break;
			else if (!result) result = r;
		}

		if (result) return result;
	} else {
		if (!node.optional && typeof(obj[node.name]) === 'undefined') {
			cause = 'missing';
		} else if (node.optional && typeof(obj[node.name]) === 'undefined') {
			// Do nothing.
		} else if (!node.nullable && obj[node.name] === null) {
			cause = 'invalid';
		} else if (node.type === 'boolean') {
			if (typeof(obj[node.name]) !== 'boolean') cause = 'invalid';
		} else if (node.type === 'string') {
			if (typeof(obj[node.name]) !== 'string') {
				cause = 'invalid';
			} else if (typeof(node.min) === 'number' && Math.floor(node.min) === node.min && obj[node.name].length < node.min) {
				cause = 'tooShort';
			} else if (typeof(node.max) === 'number' && Math.floor(node.max) === node.max && obj[node.name].length > node.max) {
				cause = 'tooLong';
			} else if (node.regex instanceof RegExp && !node.regex.test(obj[node.name])) {
				cause = 'invalid';
			} else if (typeof(node.test) === 'function' && (testResult = node.test(obj[node.name])) !== null) {
				cause = testResult;
			}
		} else if (node.type === 'number' || node.type === 'integer') {
			if (typeof(obj[node.name]) !== 'number' && obj[node.name] != parseFloat(obj[node.name])) {
				cause = 'invalid';
				console.log('#1');
			} else if (node.type === 'integer' && Math.floor(obj[node.name]) != obj[node.name]) {
				cause = 'invalid';
				console.log('#2');
			} else if (typeof(node.min) === 'number' && Math.floor(node.min) === node.min && obj[node.name] < node.min) {
				cause = 'tooSmall';
			} else if (typeof(node.max) === 'number' && Math.floor(node.max) === node.max && obj[node.name] > node.max) {
				cause = 'tooBig';
			}
		} else if (node.type === 'object') {
			cause = 'unimplemented';
		} else if (node.type === 'array') {
			cause = 'unimplemented';
		}
	}

	return cause === null ? {
		pass: true
	} : {
		pass: false,
		name: node.name,
		cause: cause
	};
}

module.exports = {
	getRequestValidator: function(options) {
		/*var example = [{
			type: 'string',
			name: 'username',
			min: 3,
			max: 16,
			regex: /^[a-zA-Z0-9_\-]*$/
		}, {
			type: 'and', // 'or'
			value: [{
				type: 'integer',
				name: 'someName',
				min: 0,
				max: 100,
				optional: true,
				nullable: true
			}, {
				type: 'number',
				name: 'wtf',
				min: 0,
				max: 100
			}, {
				type: 'boolean',
				name: 'something'
			}, {
				type: 'object',
				name: 'something',
				value: [{
					// exactly whats above
				}]
			}]
		}];*/

		return function(req, res, next) {
			for (var k in options) {
				var result = checkNode(options[k], req[k]);

				if (!result.pass) {
					console.log(result);
					return res.status(400).json({
						status: toStatus(result)
					});
				}
			}

			return next();
		}
	}
};
