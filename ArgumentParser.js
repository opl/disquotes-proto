var ArgumentParser = module.exports = function ArgumentParser(options) {
	this.options = options;
}

ArgumentParser.prototype.parse = function(str) {
	/*str = str.split(' ');

		var out = [];

		var curr = null;

		for (var s of str) {
			if (s.length === 0) {
				out.push('');
				continue;
			}

			if (s[0] === '"') {
				if (curr) throw 'Found " (' + s + ')';

				curr = s.substr(1);
			} else if (curr === null) {
				out.push(s);
			}

			if (s[s.length - 1] === '"' && ((!(s.length < 2) || s[s.length - 2] !== '\\') || (!(s.length < 3) || s[s.length - 3] === '\\'))) {
				out.push(curr + ' ' + s.substr(0, s.length - 1));
				curr = null;
			} else if (curr.indexOf(' ') !== -1) {
				curr += ' ' + s;
			}
		}

		if (curr !== null) throw 'Unclosed string.';*/

	var tokens = [];
	var token = null;

	var escaping = null;

	for (var i = 0; i < str.length; i++) {
		var c = str[i];

		if (escaping !== null) {
			escaping += c;

			if (escaping.length > 1) escaping = null;
			else if (['"', '\\', ' '].indexOf(c) === -1) throw `Unsupported escape sequence (\\${c}, ${i})`;
		} else if (c === '\\') {
			escaping = '';

			if (i === str.length - 1) throw `Unclosed escape sequence (${c}, ${i})`;

			continue;
		}

		if (!token) {
			if (c === '"' && escaping === null) {
				token = {
					type: 'longWord',
					value: ''
				};

				continue;
			} else {
				token = {
					type: 'word',
					value: ''
				};
			}
		}

		if (token.type === 'word') {
			if ((c === ' ' && escaping === null) || i === str.length - 1) {
				if (i === str.length - 1) token.value += c;

				tokens.push(token);
				token = null;
			} else {
				token.value += c;
			}
		} else if (token.type === 'longWord') {
			if (c === '"' && escaping === null) {
				tokens.push(token);
				token = null;
				if (i !== str.length - 1 && str[i + 1] !== ' ') throw `Unexpected character (${str[i + 1]}, ${i + 1})`;
				i++;
			} else {
				token.value += c;
			}
		}

		//console.log(`i=${i} c=${c} t=${token?token.type:'null'},${token?token.value:''}`);
	}

	if (token) throw `Unclosed sequence (${token.type})`;

	var out = [];

	for (var t of tokens) out.push(t.value);

	return out;
}
