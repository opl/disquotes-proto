const Discordie = require('discordie');
const ArgumentParser = require('./ArgumentParser');

// copy paste from Discordie docs <3
function fetchMessagesEx(channel, left) {
	var before = channel.messages[0];
	return channel.fetchMessages(left, before).then(e => onFetch(e, channel, left));
}

function onFetch(e, channel, left) {
	if (!e.messages.length) return Promise.resolve();
	left -= e.messages.length;
	if (left <= 0) return Promise.resolve();
	return fetchMessagesEx(channel, left);
}

module.exports = function(config, mysql, queryBuilder) {
	var client = new Discordie();

	client.connect(config.bot);

	client.Dispatcher.on('GATEWAY_READY', function(e) {
		console.log("Connected as: " + client.User.username);
	});

	client.Dispatcher.on('MESSAGE_CREATE', function(event) {
		if (/^(?:dq|disquotes)(?:$| )/.test(event.message.content)) {
			event.message.delete();
			console.log(`${event.message.author.username} (${event.message.author.id}): ${event.message.content}`);

			event.message.author.openDM().then(function(dmc) {
				var args = null;
				try {
					args = new ArgumentParser().parse(event.message.content.split(' ').slice(1).join(' '));
				} catch (ex) {
					return dmc.sendMessage(typeof(ex) === 'string' ? ex : 'Something went wrong when parsing arguments.');
				}

				console.log(args);
				if (args.length === 0 || (args.length === 1 && args[0].toLowerCase() === 'help')) {
					return dmc.sendMessage('Usage:\n`dq create <string quoteTitle> <number messageCount> [number skipCount]` Example: `dq create 20 5` will skip last 5 messages and save 20 messages that were sent before them.');
				} else if (args.length >= 1 && args[0].toLowerCase() === 'create') {
console.log('create');
					if (args[2] != parseInt(args[2]) || (args.length > 3 && args[3] != parseInt(args[3]))) return dmc.sendMessage('Invalid arguments.');

					var title = args[1];
					var count = parseInt(args[2]);
					var skip = (parseInt(args[3]) || 0) + 1;

					if (title.length < 1) return dmc.sendMessage('A quote title cannot be shorter than 1 character.');
					if (title.length > 200) return dmc.sendMessage('A quote title cannot be longer than 200 characters.');
					if (count > 200) return dmc.sendMessage('A quote cannot be longer than 200 messages.');
					if (count + skip > 2000) return dmc.sendMessage('Cannot load more than 2000 messages.');
					if (count < 1 || skip < 1) return dmc.sendMessage('Provided number is too low.');
console.log('passed arguments');

					dmc.sendMessage('Creating your quote...');

					fetchMessagesEx(event.message.channel, count + skip).then(function() {
console.log('getched');
						console.log(event.message.channel.messages.length - skip - count, event.message.channel.messages.length, event.message.channel.messages.length, count, skip);
						if (event.message.channel.messages.length - skip - count < 0 || event.message.channel.messages.length < count) return dmc.sendMessage('Requested messages don\'t exist.');

						mysql.begin(function(err, conn, commit, rollback) {
							if (err) {
								console.error(err);
								return dmc.sendMessage('Something went wrong.');
							}

							conn.query('INSERT INTO `quote` (`title`, `authorID`, `authorName`, `serverID`, `serverName`, `added`) VALUES (?, ?, ?, ?, ?, ?)', [title, event.message.author.id, event.message.author.username, event.message.guild.id, event.message.guild.name, Date.now()], function(err, result) {
								if (err) return rollback(function() {
									console.error(err);
									dmc.sendMessage('Something went wrong (2).');
								});

								var msgs = [];
								var colorCache = [];
								var roles = [].concat(event.message.guild.roles).sort((a,b) => a.position == -1 ? -1 : b.position == -1 ? 1 : a.position - b.position);

								for (var i = event.message.channel.messages.length - skip - count; i < event.message.channel.messages.length - skip; i++) {
									var msg = event.message.channel.messages[i];

									if (colorCache[msg.author.id] === undefined) {
										var m = event.message.guild.members.find(m => m.id === msg.author.id);
										for (var r of roles) {
											var ur = m.roles.find((a) => a.id === r.id);

											//console.log('user: ' + msg.author.username + ', role: ' + r.name + ', color: ' + r.color + ', m.roles: ' + JSON.stringify(m.roles) + ', has: ' + (m.roles.indexOf(r) !== -1));
											if (ur) colorCache[msg.author.id] = ur.color;
										}
										if (colorCache[msg.author.id] === 0) colorCache[msg.author.id] = null;
									}

									msgs.push([
										result.insertId,
										msg.id,
										msg.author.id,
										msg.author.username,
										colorCache[msg.author.id],
										msg.author.avatar,
										msg.content,
										new Date(msg.timestamp).getTime(),
										new Date(msg.edited_timestamp).getTime()
									]);
								}

								conn.query('INSERT INTO `message` (`quoteID`, `messageID`, `authorID`, `authorName`, `authorColor`, `avatarHash`, `text`, `added`, `edited`) VALUES ?', [msgs], function(err) {
									if (err) return rollback(function() {
										console.error(err);
										dmc.sendMessage('Something went wrong (3).');
									});

									commit(function(err) {
										if (err) return rollback(function() {
											console.error(err);
											dmc.sendMessage('Something went wrong (4).');
										});

										dmc.sendMessage('Quote created! https://disquotes.ngrok.io/q/' + result.insertId);
									});
								});
							});
						});
					});
				}
			});
		}
	});
}
