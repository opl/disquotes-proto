var Message = module.exports = function(data) {
	this.id = data.id;
	this.messageID = data.messageID;
	this.authorID = data.authorID;
	this.authorName = data.authorName;
	this.authorColor = data.authorColor;
	this.avatarHash = data.avatarHash;
	this.text = data.text;
	this.added = new Date(data.added);
	this.edited = data.edited === null ? null : new Date(data.edited);
}

Message.prototype.toPublicJSON = function() {
	return {
		id: this.id,
		messageID: this.messageID,
		authorID: this.authorID,
		authorName: this.authorName,
		authorColor: this.authorColor,
		avatarHash: this.avatarHash,
		text: this.text,
		added: this.added.getTime(),
		edited: this.edited.getTime()
	}
}

Message.getByQuoteID = function(req, quoteID, callback) {
	req.db.query('SELECT * FROM `message` WHERE `quoteID` = ? ORDER BY `added` ASC', [quoteID], function(err, fields) {
		if (err) return callback(err);

		if (fields.length === 0) return callback(null, []);

		var messages = [];

		for (var m of fields) messages.push(new Message(m));

		return callback(null, messages);
	});
}
