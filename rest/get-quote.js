const Quote = require('./../model/Quote');

module.exports = function(rest) {
	rest.get('/quote/:quoteID', rest.getRequestValidator({
		params: {
			type: 'integer',
			name: 'quoteID',
			min: 0
		}
	}), function(req, res, next) {
		Quote.getByID(req, parseInt(req.params.quoteID), function(err, quote) {
			if (err) return next(err);

			if (!quote) {
				return res.status(404).json({
					status: 'notFound'
				});
			}

			res.json({
				status: 'success',
				data: quote.toPublicJSON()
			});
		});
	});
}
