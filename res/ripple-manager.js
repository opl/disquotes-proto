(function($) {
	function clickListener(event) {
		if (event.originalEvent.rippleHandled || !$(this).hasClass('has-ripple-effect')) return;
		event.originalEvent.rippleHandled = true;

		var offset = $(this).offset();
		$(this).append(
			$('<div class="ripple-container">').append(
				$('<div class="ripple">').css({
					top: (event.pageY - offset.top - 500) + 'px',
					left: (event.pageX - offset.left - 500) + 'px'
				})
			).on('animationend webkitAnimationEnd oanimationend MSAnimationEnd', function(event) {
				if ($(event.target).is(this)) $(this).remove();
			})
		)
	}

	$('.has-ripple-effect').click(clickListener);

	new (MutationObserver || WebKitMutationObserver)(function(mutations) {
		for (var record of mutations) {
			var nodes = record.addedNodes;
			for (var i = 0; i < nodes.length; i++) {
				if ($(nodes[i]).hasClass('has-ripple-effect')) $(nodes[i]).off('click', clickListener).click(clickListener);
				$(nodes[i]).find('.has-ripple-effect').off('click', clickListener).click(clickListener);
			}
		}
	}).observe(document, {
		subtree: true,
		childList: true
	});
})(jQuery);
